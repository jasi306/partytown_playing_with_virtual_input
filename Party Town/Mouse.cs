﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;


namespace Party_Town
{//https://stackoverflow.com/questions/9330498/sendkeys-send-for-right-alt-key-any-alternatives?rq=1
 //https://docs.microsoft.com/en-us/previous-versions/windows/embedded/ms927178(v=msdn.10)?redirectedfrom=MSDN

    static class VirtualKeyboard
    {
        [DllImport("user32.dll", SetLastError = true)]
        static extern void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);
        [DllImport("user32.dll")]
        static extern uint MapVirtualKey(uint uCode, uint uMapType);

        private const int KEYEVENTF_EXTENDEDKEY = 0x0001; //Key down flag 
        private const int KEYEVENTF_KEYUP = 0x0002; //Key up flag 
        private const int VK_LMENU = 0xA4; //lAlt
        private const int VK_RMENU = 0xA5; //rAlt
        private readonly static byte[] Numeric =
        {
            0x60,
            0x61,
            0x62,
            0x63,
            0x64,
            0x65,
            0x66,
            0x67,
            0x68,
            0x69
        };

        private static void ClickByCode(byte code)
        {
            keybd_event(code, (byte)MapVirtualKey(code, 0), 0, 0);
            keybd_event(code, (byte)MapVirtualKey(code, 0), KEYEVENTF_KEYUP, 0);
        }
        public static void PressAlt()
        {
            //https://stackoverflow.com/questions/58054289/c-how-to-simulate-special-key-strokes-altnumepad
            int ilosc_zer = 4;
            for (int i = 1; i < ilosc_zer + 1; ++i)
            {
                for (int j = 0; j < Math.Pow(10, i); ++j)
                {
                    for (int k = i - 1; k >= 0; --k)
                    {
                        int p = (int)Math.Pow(10, k);
                        int d = (j / p) % 10;  //digit on k position from j;
                        ClickByCode(Numeric[d]);
                    }



                    keybd_event(0x12, (byte)MapVirtualKey(0x12, 0), 0, 0);
                    ClickByCode(Numeric[5]); //:
                    ClickByCode(Numeric[8]);
                    keybd_event(0x12, (byte)MapVirtualKey(0x12, 0), KEYEVENTF_KEYUP, 0);


                    keybd_event(0x12, (byte)MapVirtualKey(0x12, 0), 0, 0); //Alt press
                    for (int k = i - 1; k >= 0; --k)
                    {
                        int p = (int)Math.Pow(10, k);
                        int d = (j / p) % 10;  //digit on k position from j;
                        ClickByCode(Numeric[d]);
                    }
                    keybd_event(0x12, (byte)MapVirtualKey(0x12, 0), KEYEVENTF_KEYUP, 0); // Alt relese

                    ClickByCode(0x0D); //Enter
                    Thread.Sleep(12*(i+2));
                }
            }


            //https://social.msdn.microsoft.com/Forums/vstudio/en-US/5f77339e-502f-48eb-afb3-12aaacfb28ab/simulating-alt2-keypad-combination?forum=vclanguage
            //INPUT inputs = { 0 };
            //inputs.type = INPUT_KEYBOARD;

            //KEYBDINPUT ki = { 0 };
            //ki.wVk = VK_LMENU;

            //inputs.ki = ki;
            //SendInput(1, &inputs, sizeof(INPUT)); // Left alt

            //ki.wVk = VK_NUMPAD2;
            //inputs.ki = ki;
            //SendInput(1, &inputs, sizeof(INPUT)); // Numpad 2

            //ki.dwFlags = KEYEVENTF_KEYUP;

            //ki.wVk = VK_NUMPAD2;
            //inputs.ki = ki;
            //SendInput(1, &inputs, sizeof(INPUT)); // Numpad 2

            //ki.wVk = VK_LMENU;
            //inputs.ki = ki;
            //SendInput(1, &inputs, sizeof(INPUT)); // Left alt



            //keybd_event(VK_LMENU, MapVirtualKey(VK_LMENU, 0), 0, 0);
            //keybd_event(Numeric[2], MapVirtualKey(Numeric[2], 0), 0, 0);

            //keybd_event(Numeric[2], MapVirtualKey(Numeric[2], 0), KEYEVENTF_KEYUP, 0);
            //keybd_event(VK_LMENU, MapVirtualKey(VK_LMENU, 0), KEYEVENTF_KEYUP, 0);






            //keybd_event(VK_RMENU, 0, KEYEVENTF_EXTENDEDKEY, 0);
            // keybd_event(VK_LMENU, 0, KEYEVENTF_EXTENDEDKEY, 0);
            // Thread.Sleep(10);
            // keybd_event((byte)Numeric[5], 0, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);
            // Thread.Sleep(10);
            // keybd_event((byte)Numeric[6], 0, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);
            // Thread.Sleep(10);
            // //keybd_event((byte)Numeric[4], 0, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);
            // Thread.Sleep(10);
            //// keybd_event((byte)Numeric[8], 0, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);
            // Thread.Sleep(10);


            // keybd_event(VK_LMENU, 0, KEYEVENTF_KEYUP, 0);
            //keybd_event(VK_LMENU, 0, KEYEVENTF_EXTENDEDKEY, 0);

            //keybd_event((byte)Numeric[5], 0, KEYEVENTF_EXTENDEDKEY, 0);

            //keybd_event((byte)Numeric[6], 0, KEYEVENTF_EXTENDEDKEY , 0);

            //keybd_event((byte)Numeric[4], 0, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);

            // keybd_event((byte)Numeric[8], 0, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);

            //keybd_event(VK_LMENU, 0, KEYEVENTF_KEYUP, 0);
            //keybd_event((byte)Numeric[5], 0, KEYEVENTF_KEYUP, 0);

            //keybd_event((byte)Numeric[6], 0, KEYEVENTF_KEYUP, 0);


        }
        public static void reliseAlt()
        {
            //keybd_event(VK_RMENU, 0, KEYEVENTF_KEYUP, 0);
        }

    }

    class VirtualMouse
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void mouse_event(uint dwFlags, uint dx, uint dy, uint cButtons, uint dwExtraInfo);
        private const int MOUSEEVENTF_LEFTDOWN = 0x02;
        private const int MOUSEEVENTF_LEFTUP = 0x04;
        private const int MOUSEEVENTF_RIGHTDOWN = 0x08;
        private const int MOUSEEVENTF_RIGHTUP = 0x10;

        private Cursor Cursor;

        public VirtualMouse()
        {
            this.Cursor = new Cursor(Cursor.Current.Handle);
        }

        public void Move(int X, int Y)
        {
            Cursor.Position = new Point(X, Y);
        }
        public void Click()
        {
            mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, (uint)Cursor.Position.X, (uint)Cursor.Position.Y, 0, 0);
        }
        public void Hold()
        {
            mouse_event(MOUSEEVENTF_LEFTDOWN, (uint)Cursor.Position.X, (uint)Cursor.Position.Y, 0, 0);

        }
        public void Relise()
        {
            mouse_event(MOUSEEVENTF_LEFTUP, (uint)Cursor.Position.X, (uint)Cursor.Position.Y, 0, 0);

        }
        public string GetLocation()
        {
            return " " + Cursor.Position.X + " " + Cursor.Position.Y;
        }
    }
}